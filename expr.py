from node import *

class Expr(Node):
    def __init__(self):
        Node.__init__(self)


class RelExpr(Expr):
    def __init__(self, relop, e1, e2):
        Expr.__init__(self)
        self.relop = relop
        self.e1 = e1
        self.e2 = e2

    def compile(self):
        if self.true == fall:
            self.code = "ifNot {} {} {} goto {}\n".format(
                self.e1, self.relop, self.e2, label(self.false)
            )
        elif self.false == fall:
            self.code = "if {} {} {} goto {}\n".format(
                self.e1, self.relop, self.e2, label(self.true)
            )
        else:
            self.code = "if {} {} {} goto {}\ngoto {}\n".format(
                self.e1, self.relop, self.e2, label(self.true), label(self.false)
                )


class OrExpr(Expr):
    def __init__(self, b1, b2):
        Expr.__init__(self)
        self.b1 = b1
        self.b2 = b2

    def compile(self):
        # fig. 6.40
        #self.b1.true = self.true if self.true != fall else newlabel()
        #self.b1.false = fall

        self.b1.true = self.true if self.true != fall else newlabel()
        self.b1.false = fall

        self.b2.true = self.true
        self.b2.false = self.false

        self.b1.compile()
        self.b2.compile()
        if self.true != fall:
            self.code = self.b1.code + self.b2.code
        else:
            self.code = self.b1.code + self.b2.code + label(self.b1.true)


class AndExpr(Expr):
    def __init__(self, b1, b2):
        Expr.__init__(self)
        self.b1 = b1
        self.b2 = b2

    def compile(self):
        self.b1.true = fall
        self.b1.false = self.false if self.false != fall else newlabel()

        self.b2.true = fall
        self.b2.false = self.false
        self.b1.compile()
        self.b2.compile()
        self.code = self.b1.code + self.b2.code


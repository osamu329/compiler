from node import *

class Stmt(Node):
    def __init__(self):
        Node.__init__(self)
        self.next = None

class SimpleStmt(Stmt):
    def __init__(self, code):
        Stmt.__init__(self)
        self.code = code

    def compile(self):
        pass

class IfStmt(Stmt):
    def __init__(self, cond, body, elseBody=None):
        Stmt.__init__(self)
        self.cond = cond
        self.body = body
        self.elseBody = elseBody

    def compile(self):
        cond, body, elseBody = self.cond, self.body, self.elseBody
        if elseBody is None:
            cond.true = newlabel()
            cond.false = self.next
            cond.compile()
            body.compile()
            self.code = cond.code + label(cond.true) +":\n" + body.code
        else:
            # if pred:
            #     body
            # else:
            #     elseBody
            cond.true = newlabel()
            cond.false = newlabel()
            cond.compile()
            body.compile()
            elseBody.compile()
            self.code = (cond.code
                       + label(cond.true) + ":\n"
                       + body.code
                       + label(cond.false) + ":\n"
                       + elseBody.code())


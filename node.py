_label = 0
fall = object()

def newlabel():
    global _label
    _label+=1
    return _label


def label(l):
    return "L%d" % l


class Node:
    def __init__(self):
        self.true = None
        self.false = None
        self.code = ""


class Program(Node):
    def __init__(self, stmt):
        global _label
        _label = 0
        self.stmt = stmt

    def compile(self):
        self.stmt.next = newlabel()
        self.stmt.compile()
        self.code = self.stmt.code + label(self.stmt.next) + ":"

    def print(self):
        for x in self.code.split("\n"):
            if x.startswith("L") and x.endswith(":"):
                print(x)
            else:
                print("    %s" % x)

from stmt import *
from expr import *
from node import Program

print("OR")
prog = Program(
    IfStmt(
        OrExpr(
            RelExpr("<", "x", "100"),
            RelExpr(">", "x", "100")),
        SimpleStmt("x = 1\n") )
    )
prog.compile()
prog.print()

print()
print("AND")
prog = Program(
    IfStmt(
        AndExpr(
            RelExpr("<", "x", "100"),
            RelExpr(">", "x", "100")),
        SimpleStmt("x = 1\n") )
    )
prog.compile()
prog.print()

print()
print("OR-AND")
prog = Program(
    IfStmt(
        OrExpr(
            RelExpr("<", "x", "100"),
            AndExpr(
                RelExpr("<", "x", "100"),
                RelExpr("!=", "x", "y")
            )
        ),
        SimpleStmt("x = 1\n") )
    )
prog.compile()
prog.print()

